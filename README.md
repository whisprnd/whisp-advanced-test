# Whisp test

## Instruction
1. Clone this repository
2. Install dependencies using `npm install` or `yarn` command.
3. Start project with `npm run start` or `yarn start`
4. The exercise will be displayed in the app's main page.

Good luck!